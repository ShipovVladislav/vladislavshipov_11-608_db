﻿
CREATE TABLE buyer(
       id SERIAL PRIMARY KEY  NOT NULL UNIQUE,
       first_name VARCHAR(20),
       last_name VARCHAR(30),
       phone_number VARCHAR(15),
       mail_address VARCHAR(30));

CREATE TABLE employee(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       first_name VARCHAR(20),
       last_name VARCHAR(30),
       date_of_birth DATE,
       salary INTEGER);
INSERT INTO employee(first_name,last_name) values('John','Smith'),('Paul','Smith');
CREATE TABLE publishing_house(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       name VARCHAR(50),
       address VARCHAR(50),
       rate REAL,
       CHECK(rate>0),
       CHECK(rate<10));
CREATE TABLE book(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       genre VARCHAR(20),
       name VARCHAR(50),
       authors VARCHAR(50),
       publishing_house_id INTEGER REFERENCES publishing_house(id),
       price INTEGER);
INSERT INTO book (genre,name,authors,price) VALUES ('sci-fi','Do Androids Dream of Electric Sheep?','Philip K. Dick',1000);
INSERT INTO book (name) VALUES ('Book');
SELECT * FROM book;
CREATE TABLE purchase(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       buyer_id INTEGER REFERENCES buyer(id),
       courier_id INTEGER REFERENCES employee(id),
       order_date DATE,
       delivery_date DATE,
       delivary_type VARCHAR(20),
       delivery_price VARCHAR(20),
       delivery_address VARCHAR(100), 
       note TEXT);
INSERT INTO purchase(order_date,delivery_date) VALUES('2012-02-12','2012-02-18'),(now(),now());
SELECT * FROM purchase;
CREATE TABLE books_in_order(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       purchase_id INTEGER REFERENCES purchase(id),
       book_id INTEGER REFERENCES book(id),
       count_of_books INTEGER);
INSERT INTO books_in_order (purchase_id,book_id) VALUES(1,2);
SELECT * FROM books_in_order;

CREATE TABLE store(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       address VARCHAR(100),
       manager_id INTEGER REFERENCES employee(id) UNIQUE);


CREATE TABLE books_in_store(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       store_id INTEGER REFERENCES store(id),
       book_id INTEGER REFERENCES book(id),
       count_of_books INTEGER);


//Заказы за последнюю неделю
CREATE VIEW orders_per_week AS
    SELECT * FROM purchase
         WHERE order_date> now()-interval '30 days';
Select * from orders_per_week;

CREATE INDEX orders ON purchase(order_date);
     
//Доставки за последнюю неделю
CREATE VIEW delivery_per_week AS
     SELECT * FROM purchase
          WHERE delivery_date> now()-interval '7 days';   
CREATE INDEX deliverys ON purchase(delivery_date);


//10 самых популярных книг
CREATE VIEW most_popular_books AS
     SELECT b.name,count(books_in_order.id) 
          FROM books_in_order
          INNER JOIN book as b on book_id=b.id
          GROUP BY b.name LIMIT(10);
CREATE INDEX popular_books ON book_in_order(book_id);


CREATE TABLE books_in_order_denormalized(
       id SERIAL PRIMARY KEY NOT NULL UNIQUE,
       purchase_id INTEGER REFERENCES purchase(id),
       book_id INTEGER REFERENCES book(id),
       count_of_books INTEGER,
       genre VARCHAR(20),
       name VARCHAR(50),
       authors VARCHAR(50),
       publishing_house_id INTEGER REFERENCES publishing_house(id),
       price INTEGER);



SELECT * FROM books_in_order_denormalized;


CREATE OR REPLACE FUNCTION denormal() returns void
AS $$
BEGIN
DELETE FROM books_in_order_denormalized;
CREATE TEMPORARY TABLE t_temp as (
     SELECT books_in_order.id,purchase_id,b.id as book_id,count_of_books,genre,name,authors,publishing_house_id,price
         FROM books_in_order
         INNER JOIN book as b on book_id=b.id);
INSERT INTO books_in_order_denormalized 
        (id,purchase_id,book_id,count_of_books,genre,name,authors,publishing_house_id,price)
         SELECT * From t_temp;     
END; $$
LANGUAGE plpgsql;
   

SELECT * FROM book WHERE lower(name) = 'book';
CREATE INDEX to_lower ON book (lower(name));


SELECT * FROM employee WHERE (first_name || ' ' || last_name) = 'John Smith';
CREATE INDEX people_names ON employee ((first_name || ' ' || last_name));

CREATE TABLE employee_hired_log(
       id SERIAL PRIMARY KEY,
       first_name TEXT,
       last_name TEXT,
       hire_date DATE);
CREATE TABLE employee_dismissed_log(
       id SERIAL PRIMARY KEY,
       first_name TEXT,
       last_name TEXT,
       dismissed_date DATE);

CREATE FUNCTION hire_insert() RETURNS trigger AS
$BODY$
BEGIN
  INSERT INTO employee_hired_log (first_name,last_name,hire_date) values( NEW.first_name,NEW.last_name,now());
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql

CREATE FUNCTION dismiss_insert() RETURNS trigger AS
$BODY$
BEGIN
  INSERT INTO employee_dismissed_log (first_name,last_name,dissmissed_date) values( OLD.first_name,OLD.last_name,now());
  RETURN OLD;
END;
$BODY$ LANGUAGE plpgsql


CREATE TRIGGER employee_hired
AFTER INSERT ON employee
FOR EACH ROW
EXECUTE PROCEDURE hire_insert();

CREATE TRIGGER employee_dismissed
BEFORE DELETE ON employee
FOR EACH ROW
EXECUTE PROCEDURE dismiss_insert();

