﻿

SELECT department.name from department 
INNER JOIN employee as emp on department.id=emp.department_id 
where (select count(name) from employee as E where E.department_id=department.id)>3
group by department.name;

SELECT department.name 
from department 
INNER JOIN employee as emp on department.id=emp.department_id
GROUP BY department.name 
HAVING count(emp.id)<3;

SELECT name
FROM employee as emp
WHERE department_id<>(select department_id from employee E where E.id=emp.manager_id );

WITH max_salary as (SELECT department_id,sum(salary) from employee  as e GROUP BY department_id)
SELECT dep.name from department as dep
INNER JOIN max_salary on dep.id=max_salary.department_id
WHERE sum=(select max(sum) from max_salary);



                         