﻿CREATE TABLE employee (id INTEGER PRIMARY KEY,name text,salary INTEGER,manager_id INTEGER);
INSERT INTO employee (id,name,salary) VALUES (1,'Вася',5);
INSERT INTO employee (id,name,salary) VALUES (2,'Кирилл',10);
INSERT INTO employee (id,name,salary) VALUES (3,'Артем',10);
INSERT INTO employee (id,name,salary) VALUES (4,'Коля',10);
INSERT INTO employee (id,name,salary) VALUES (6,'Босс',200);
SELECT * FROM employee;
DELETE FROM employee WHERE name='Вася';
UPDATE employee SET manager_id=6 WHERE id=2;
SELECT * FROM employee as emp;
                         