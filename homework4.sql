﻿

create table genre(
id INTEGER PRIMARY KEY ,
name varchar(20));

create table studio(
id INTEGER PRimary key,
name varchar(20));

CREATE TABLE film (
id INTEGER PRIMARY KEY ,
title varchar(20),
year INTEGER,
studio_id INTEGER references genre(id),
length integer,
genre_id INTEGER references genre(id));
cost INTEGER);

CREATE TAble actor (
id INTEGER PRIMARY KEY ,
name varchar(20));

CREATE TABLE filmstar(
actor_id varchar(20) references actor(id),
film_id varchar(20)references film(id),
starsmoney INTEGER);


CREATE TABLE cinema(
id INTEGER PRIMARY KEY,
name VARCHAR(20));

CREATE TABLE session(
id INTEGER PRIMARY KEY,
cinema_id INTEGER REFERENCES cinema(id),
film_id INTEGER REFERENCES film(id),
date VARCHAR(20),
price INTEGER);
