﻿

CREATE TABLE current (
      clusterID INTEGER,
      nodeID INTEGER,
      manufacturer TEXT,
      PRIMARY KEY(clusterID,nodeID));


      
CREATE TABLE log1 (
      id SERIAL PRIMARY KEY,
      clusterID INTEGER,
      nodeID INTEGER,
      manufacturer TEXT,
      add_date date
      );
CREATE TABLE log2 (
      id SERIAL PRIMARY KEY,
      clusterID INTEGER,
      nodeID INTEGER,
      manufacturer TEXT,
      removed bool
      );




INSERT INTO log1 (clusterID, nodeID, manufacturer,add_date) SELECT clusterID, nodeID, manufacturer,now()From current;
CREATE TEMPORARY TABLE t_temp as (
     SELECT MIN(id) as min_id,clusterID, nodeID, manufacturer,MIN(add_date) FROM log1
     GROUP BY clusterID,nodeID,manufacturer);
DELETE FROM log1;
INSERT INTO log1 (clusterID, nodeID, manufacturer,add_date) SELECT clusterID, nodeID, manufacturer,min From t_temp;



UPDATE log2 SET removed=true;
INSERT INTO log2 (clusterID, nodeID, manufacturer,removed) SELECT clusterID, nodeID, manufacturer,false From current;
UPDATE log2 SET removed=false WHERE (SELECT COUNT(*) from (SELECT * from current where nodeID=log2.nodeID) as cur where clusterID=log2.clusterID)=1;
CREATE TEMPORARY TABLE t_temp1 as 
     SELECT MAX(id) as min_id,clusterID, nodeID, manufacturer,removed FROM log2
     GROUP BY clusterID,nodeID,manufacturer,removed;
DELETE FROM log2;
INSERT INTO log2 (clusterID, nodeID, manufacturer,removed) SELECT clusterID, nodeID, manufacturer,min From t_temp1;



SELECT nodeID from log1 WHERE add_date>now()-interval '10 days' GROUP BY nodeID;

SELECT nodeID from log2 WHERE removed=false GROUP BY nodeID LIMIT(10);






   